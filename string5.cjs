const fox = (fox) => {
    if (fox.length === 0)
        return 0;
    else {
        let combined = ""
        fox.forEach(element => {
            combined += element + " ";
        });
        return combined;
    }
}

module.exports = fox
