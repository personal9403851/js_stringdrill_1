const sol1 = (num) => {
    let numericValues = num.map(element => {
        return element.replace(/[^0-9.-]/g, '')
    });
    numericValues = numericValues.map(element => {
        return parseFloat(element)
    })
    return numericValues
}

module.exports = sol1