const sol2 = (ipAddress) => {
    
    let ipNumeric = ipAddress.split(".")
    ipNumeric = ipNumeric.map((item) => {
        return parseInt(item)
    })
    return ipNumeric
}

module.exports = sol2